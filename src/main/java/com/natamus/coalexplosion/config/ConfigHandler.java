package com.natamus.coalexplosion.config;

import com.google.common.collect.Lists;
import net.minecraftforge.common.ForgeConfigSpec;

import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class ConfigHandler
{
    private static final ForgeConfigSpec.Builder BUILDER = new ForgeConfigSpec.Builder();
    public static General GENERAL = new General(BUILDER);
    public static ForgeConfigSpec spec = BUILDER.build();

    public static class General
    {
        public ForgeConfigSpec.ConfigValue<Double> explosionRange;
        public ForgeConfigSpec.ConfigValue<Boolean> causeFire,leftClickTorch,debugMode;
        public static ForgeConfigSpec.ConfigValue<List<? extends String>> sparkerThings;
        public static ForgeConfigSpec.ConfigValue<List<? extends String>> sparkerTags;

        public General(ForgeConfigSpec.Builder builder)
        {
            Predicate<Object> validator = o -> o instanceof String;

            builder.push("General");
            explosionRange = builder
                    .comment("The range of the explosion of a coal block.")
                    .defineInRange("explosionRange", 1.0, 0.0, 256.0);
            causeFire = builder
                    .comment("Whether an exploding coal block should cause fire.")
                    .define("causeFire", false);
            sparkerThings = builder
                    .comment("List of items and #tags, that when left clicked on a coal vein, will set it off:\nexample: \"minecraft:iron_pickaxe\",\"minecraft:flint\"\n don't include torches here, instead enable them")
                    .defineListAllowEmpty(Collections.singletonList("sparkerThings"),() -> Lists.newArrayList(), validator);
            sparkerTags = builder
                    .comment("NOT USED FOR NOW, I'M HAVING ISSUES WRAPPING MY TINY BRAIN AROUND IT.\nList of items and #tags, that when left clicked on a coal vein, will set it off:\nexample: \"#stick\", \"#pickaxe\", \"#ingotiron\"\n don't include torches here, instead enable them")
                    .defineListAllowEmpty(Collections.singletonList("sparkersTags"),() -> Lists.newArrayList(), validator);
            leftClickTorch = builder
                    .comment("Should torches when left clicked on a block cause an explosion?")
                    .define("leftClickTorch",false);
            debugMode = builder
                    .comment("Turn on in game debug messages.")
                    .define("debugMode",false);
            builder.pop();
        }
    }
}