[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

Join at least [![](https://img.shields.io/discord/96753964485181440.svg?colorB=7289DA)](https://discord.gg/jMvUW6V) of us on Discord right now!

## Explosive Coal

Making coal irritating to your players. :)

**Configurable
****Range** (default = 5.0, min 0 max 256.0): The range of the explosion of a coal block.

**Strength** (default = 10.0, min 0, max 256.0): The amount of damage the explosions do.
**causeFire** (default = false): Whether an exploding coal block should cause fire.

**Sparks **(default = iron and gold pickaxes): Whatever you add to this list will trigger explosions like Pigporker below was expecting.

**TODO:**

1.16.5 and 1.17.1 updates to the mod based what I did for 1.12.2 today August 7 2021. Other MC versions upon request only.

chaining as described in the future ideas and features below and to configure all of those chances.

Future ideas and features:
The idea is, you would have to use non-metal and non-stone (did you know that stone is actually shitty metal, or that metal is actually quality stone? factoid or factlet you decide). so wood or diamond is the ideal tool.

~I'll add a config to it.~

There'll be a base explosion chance. Or maybe ... there's a base chance it catches on fire. Then while it's burning there's a base chance it becomes one of those magma blocks from the nether, and another base chance that it might explode. If the fire is next to a coal block, it will just go ahead and catch on fire. So, you could end up burning out a vein. Regular torches will ignite them with a chance each tick update.
Actually, I've been thinking about it a long time, but it finally came together in my mind.

And of course, the more magma blocks that are next to it and the morning burning blocks there are, the greater the chance for the explosion, and then when it does explode the adjacent existing coal blocks will increase that.

.
That will add fun to using iron and gold pickaxes on coal veins or well deliberate things, i can see folks getting silk touch just be a special kind of player.

So the config will contain these values:

a list of allowed tools for harvesting without triggering a calamity, default will be wood and diamond.

there will be no blacklist, it's either exempt or it's not, if it is not exempt the following get checked from the config.

base chance to ignite

base chance after onfire to become magma block

base chance after onfire to explode

chance increase amt if neighbor is on fire to explode

chance increase amt if neightbor is magma block to explode

base chance to increase explosion if neighbor is coal

base chance to increase explosion if neighbor is flammable

base chance to increase explosion if neighbor is magma block

the following will be used to calculation explosion if chances allow it

coal explosion increase amt (for each adjacent coal ore, this is the increase)

flammable explosion increase amt (for each adjacent flammable, this is the increase)

magma explosion increase amt (for each adjacent magma, this is the increase)

I see that lava probably should be considered, but it's everywhere, so i'll add config to allow it, by default we'll ignore it, if it were turned on all coal veins touching lava will be hit ignited and you can guess what that will do, lots of explosion lag, at least the lava will destroy most of the entities. If the lava is allowed it'll be treated as a magma block.

Logo Design by [x\_fade13](https://www.curseforge.com/members/x_fade13/projects)
